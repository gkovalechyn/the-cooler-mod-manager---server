import { Controller, Post, Param } from "@nestjs/common";

@Controller("admin")
export class ManagementController {
  @Post("build/:repository")
  public build(
    @Param("repository")
    repository: string
  ) {
    // Build the repository
  }

  @Post("create/:repository")
  public createRepository(
    @Param("repository")
    repository: string
  ) {
    // Create a new repository
  }
}
