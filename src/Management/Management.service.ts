import { Injectable } from "@nestjs/common";
import { ConfigurationService } from "../Configuration/ConfigurationService";
import { RepositoryService } from "../Repository/Repository.service";

@Injectable()
export class ManagementService {
  public constructor(
    private readonly configurationService: ConfigurationService,
    private readonly repositoryService: RepositoryService
  ) {}
}
