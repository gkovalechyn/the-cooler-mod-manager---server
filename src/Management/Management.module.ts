import { Module } from "@nestjs/common";
import { ManagementController } from "./Management.controller";
import { ManagementService } from "./Management.service";
import { RepositoryModule } from "../Repository/Repository.module";
import { ConfigurationModule } from "../Configuration/ConfigurationModule";

@Module({
  controllers: [ManagementController],
  imports: [ConfigurationModule, RepositoryModule],
  providers: [ManagementService],
  exports: [ManagementService]
})
export class ManagementModule {}
