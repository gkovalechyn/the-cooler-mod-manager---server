import { IsNumber, IsOptional, IsBoolean, IsString, IsNotEmpty, IsIn } from "class-validator";
import { Transform } from "class-transformer";
import { RepositoryState } from "./RepositoryState";
import { Environment, ALL_ENVIRONMENTS } from "./Environment";
import { FileInfo } from "./FileInfo";

export class FileRequestQueryDTO {
  @IsOptional()
  @IsNumber()
  @Transform(value => Number(value))
  public offset?: number;

  @IsOptional()
  @IsIn(ALL_ENVIRONMENTS)
  @IsString()
  public environment?: Environment;
}

export class FileStatsQueryDTO {
  @IsOptional()
  @IsIn(ALL_ENVIRONMENTS)
  @IsString()
  public environment?: Environment;
}

export class DetailsRequestQueryDTO {
  @IsOptional()
  @IsIn(ALL_ENVIRONMENTS)
  @IsString()
  public environment?: Environment;
}

export class CreateRepositoryDTO {
  @IsString()
  @IsNotEmpty()
  public name: string;
}

export class RepositoryDTO {
  public name: string;
  public state: RepositoryState;
  public version: number;
  public items: Record<string, FileInfo>;

  public constructor(name = "", state = RepositoryState.PENDING_SCAN, version: number, items = {}) {
    this.name = name;
    this.state = state;
    this.version = version;
    this.items = items;
  }
}

export class RepositoryStateDTO {
  public name: string;
  public version: number;
  public state: RepositoryState;

  public constructor(name = "", state = RepositoryState.PENDING_SCAN, version: number) {
    this.name = name;
    this.version = version;
    this.state = state;
  }
}
