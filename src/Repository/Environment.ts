export enum Environment {
  STAGING = "staging",
  PRODUCTION = "production"
}

export const ALL_ENVIRONMENTS = [Environment.PRODUCTION, Environment.STAGING];
