import { Runnable } from "../Task/Runnable";
import { NotifyCallback } from "../Task/NotifyCallback";
import { Repository } from "./Repository";
import { FileInfo } from "./FileInfo";
import { readdir, createReadStream, stat } from "fs";
import { promisify } from "util";
import { createHash } from "crypto";
import { Environment } from "./Environment";

export class BuildRepositoryRunnable extends Runnable {
  public constructor(
    private readonly repository: Repository,
    private readonly repositoryPath: string,
    private readonly environment = Environment.PRODUCTION
  ) {
    super();
  }

  async run(notify: NotifyCallback): Promise<void> {
    this.repository.items[this.environment] = {};

    notify(`Build started at: ${new Date().toISOString()}`);

    await this.scanModTree(
      this.repositoryPath + `/${this.environment}`,
      notify,
      "",
      this.repository.items[this.environment]
    );

    this.repository.version++;

    notify(`Build finished at: ${new Date().toISOString()}`);
  }

  private async scanModTree(
    path: string,
    notify: NotifyCallback,
    relativePath: string,
    output: Record<string, FileInfo>
  ) {
    const readDirAsync = promisify(readdir);
    const statAsync = promisify(stat);

    const entries = await readDirAsync(path, { withFileTypes: true });
    for (const entry of entries) {
      const entryPath = path + "/" + entry.name;

      if (entry.isDirectory()) {
        notify(`Scanning directory "${entry.name}"`);
        await this.scanModTree(entryPath, notify, `${relativePath}/${entry.name}`, output);
      } else {
        const item = new FileInfo();
        item.name = entry.name;
        item.hash = await this.calculateMD5(entryPath);
        item.size = await (await statAsync(entryPath)).size;

        // Remove the leading "/"
        const pathToSave = `${relativePath}/${entry.name}`.slice(1);

        notify(`${pathToSave} => ${item.hash}`);
        output[`${pathToSave}`] = item;
      }
    }
  }

  private async calculateMD5(path: string): Promise<string> {
    const md5 = createHash("md5");
    md5.setEncoding("hex");

    const promise = new Promise((resolve, reject) => {
      const fileStream = createReadStream(path);
      fileStream.pipe(md5);
      fileStream.on("end", () => {
        resolve(md5.read());
      });

      fileStream.on("error", reject);
    });

    return (await promise) as string;
  }
}
