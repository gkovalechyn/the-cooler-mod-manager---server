import { RepositoryState } from "./RepositoryState";
import { FileInfo } from "./FileInfo";
import { Environment } from "./Environment";

export class Repository {
  public name: string;
  public state: RepositoryState = RepositoryState.PENDING_SCAN;
  public version = 0;

  public items: Record<Environment, Record<string, FileInfo>> = {
    production: {},
    staging: {}
  };

  public constructor(name: string) {
    this.name = name;
  }

  public itemExists(path: string, environment = Environment.PRODUCTION) {
    return !!this.getItem(path, environment);
  }

  public getItem(path: string, environment = Environment.PRODUCTION) {
    const environmentItems = this.items[environment];
    return environmentItems[path];
  }

  public getEnvironmentItems(environment: Environment) {
    return this.items[environment] || {};
  }
}
