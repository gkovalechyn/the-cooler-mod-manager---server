import {
  Controller,
  Get,
  Body,
  Param,
  Query,
  Post,
  Res,
  NotFoundException,
  UnprocessableEntityException,
  NotImplementedException,
  Header
} from "@nestjs/common";
import {
  FileRequestQueryDTO,
  DetailsRequestQueryDTO,
  CreateRepositoryDTO,
  FileStatsQueryDTO,
  RepositoryDTO
} from "./Repository.dtos";
import { RepositoryService } from "./Repository.service";
import sanitizeFilename from "sanitize-filename";
import { Response } from "express";
import { RepositoryStateDTO } from "./Repository.dtos";
import { Environment } from "./Environment";

@Controller("repositories")
export class RepositoryController {
  public constructor(private readonly repositoryService: RepositoryService) {}

  @Post()
  public async createRepository(
    @Body()
    body: CreateRepositoryDTO
  ) {
    const repository = this.repositoryService.getRepository(body.name);

    if (repository) {
      throw new UnprocessableEntityException();
    }

    return await this.repositoryService.createRepository(body.name);
  }

  @Post(":name/build")
  public async buildRepository(
    @Param("name")
    name: string,
    @Query("environment")
    environment: Environment
  ) {
    const repository = this.repositoryService.getRepository(name);
    environment = environment || Environment.PRODUCTION;

    if (!repository) {
      throw new NotFoundException();
    }

    if (!repository.items[environment]) {
      throw new NotFoundException();
    }

    const task = await this.repositoryService.buildRepository(repository, environment);
    return {
      taskId: task.Id
    };
  }

  @Post(":name/depoy")
  public async deployRepository(
    @Param("name")
    name: string
  ) {
    const repository = this.repositoryService.getRepository(name);

    if (!repository) {
      throw new NotFoundException();
    }

    throw new NotImplementedException();
  }

  @Get([":name", ":name/details"])
  public getDetails(
    @Param("name")
    name: string,
    @Query()
    query: DetailsRequestQueryDTO
  ) {
    const repository = this.repositoryService.getRepository(name);

    if (!repository) {
      throw new NotFoundException();
    }

    const environment = query.environment || Environment.PRODUCTION;

    return new RepositoryDTO(
      repository.name,
      repository.state,
      repository.version,
      repository.getEnvironmentItems(environment)
    );
  }

  @Get(":name/file/:file")
  @Header("Content-Type", "application/octet-stream")
  public getFile(
    @Param("name")
    name: string,
    @Param("file")
    file: string,
    @Query()
    query: FileRequestQueryDTO,
    @Res()
    res: Response
  ) {
    const repository = this.repositoryService.getRepository(name);
    const environment = query.environment || Environment.PRODUCTION;
    const safeFile = sanitizeFilename(file);
    const path = safeFile.split(";").join("/");
    const offset = query.offset || 0;

    if (!repository) {
      throw new NotFoundException("Repository not found");
    }

    if (!repository.itemExists(path, environment)) {
      throw new NotFoundException("File not found");
    }

    const stream = this.repositoryService.getStreamFor(repository, path, offset, environment);
    stream.pipe(res, {
      end: true
    });
  }

  @Get(":name/file/:file/stats")
  public async getFileStats(
    @Param("name")
    name: string,
    @Param("file")
    file: string,
    @Query()
    query: FileStatsQueryDTO
  ) {
    const repository = this.repositoryService.getRepository(name);
    const environment = query.environment || Environment.PRODUCTION;
    const safeFile = sanitizeFilename(file);
    const path = safeFile.split(";").join("/");

    if (!repository.itemExists(path, environment)) {
      throw new NotFoundException();
    }

    return await this.repositoryService.getFileStats(repository, path, environment);
  }

  @Get()
  public getRepositories() {
    const repositories = this.repositoryService.getRepositories();
    const result = new Array<RepositoryStateDTO>();

    for (const name of Object.keys(repositories)) {
      const repository = repositories[name];

      result.push(new RepositoryStateDTO(name, repository.state, repository.version));
    }

    return result;
  }
}
