export enum RepositoryState {
  PENDING_SCAN = "pending-scan",
  SCANNING = "scanning",
  READY = "ready"
}
