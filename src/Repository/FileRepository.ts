import { RepositoryState } from "./RepositoryState";
export interface FileRepository {
  name: string;
  version: number;
  state: RepositoryState;
}
