export class FileInfo {
  public name: string;
  public hash: string;
  public size: number;

  public constructor(name = "", size = 0, hash = "") {
    this.name = name;
    this.size = size;
    this.hash = hash;
  }
}
