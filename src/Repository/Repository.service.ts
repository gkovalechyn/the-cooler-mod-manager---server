import { Injectable, Logger } from "@nestjs/common";
import { ConfigurationService } from "../Configuration/ConfigurationService";
import { Repository } from "./Repository";
import {
  exists,
  mkdir,
  createReadStream,
  writeFileSync,
  readFileSync,
  existsSync,
  writeFile,
  readFile,
  stat
} from "fs";
import { promisify } from "util";
import { RepositoryState } from "./RepositoryState";
import { FileRepository } from "./FileRepository";
import { TaskService } from "../Task/Task.service";
import { BuildRepositoryRunnable } from "./BuildRepositoryRunnable";
import { Environment, ALL_ENVIRONMENTS } from "./Environment";
import { FileInfo } from "./FileInfo";

@Injectable()
export class RepositoryService {
  private repositories: Record<string, Repository> = {};
  private readonly repositoryFilePath;
  private logger = new Logger("RepositoryService");

  public constructor(
    private readonly configurationService: ConfigurationService,
    private readonly taskService: TaskService
  ) {
    this.repositoryFilePath = configurationService.repositoriesPath + "repositories.json";
    this.loadRepositories();
  }

  public async loadRepositories() {
    const existsAsync = promisify(exists);
    const readFileAsync = promisify(readFile);

    if (await existsAsync(this.repositoryFilePath)) {
      const fileRepositories = JSON.parse(
        await readFileAsync(this.repositoryFilePath, { encoding: "utf8" })
      ) as FileRepository[];

      for (const repo of fileRepositories) {
        this.repositories[repo.name] = new Repository(repo.name);
        this.repositories[repo.name].version = repo.version || 0;
        this.repositories[repo.name].state = repo.state;
      }
    }

    for (const name of Object.keys(this.repositories)) {
      const repository = this.repositories[name];
      const repositoryPath = this.configurationService.repositoriesPath + repository.name;

      for (const environment of ALL_ENVIRONMENTS) {
        if (await existsAsync(repositoryPath + `/${environment}.json`)) {
          repository.items[environment] = JSON.parse(
            await readFileAsync(repositoryPath + `/${environment}.json`, { encoding: "utf8" })
          );
        }
      }

      this.logger.debug(`Loaded repository "${name}"`);
    }
  }

  public async saveRepositories() {
    const toSave: FileRepository[] = [];
    const writeFileAsync = promisify(writeFile);

    for (const name of Object.keys(this.repositories)) {
      const repo = this.repositories[name];

      toSave.push({
        name,
        version: repo.version,
        state: repo.state
      });

      const repositoryPath = this.configurationService.repositoriesPath + name;

      for (const environment of Object.keys(repo.items)) {
        const items = repo.items[environment];
        await writeFileAsync(repositoryPath + `/${environment}.json`, JSON.stringify(items));
      }
    }

    return writeFileAsync(this.repositoryFilePath, JSON.stringify(toSave));
  }

  public getRepository(name: string): Repository | null {
    return this.repositories[name];
  }

  public async createRepository(name: string) {
    if (this.getRepository(name)) {
      throw new Error(`Another repository with the name "${name}" already exists`);
    }

    const repository = new Repository(name);
    const rootPath = this.configurationService.repositoriesPath + name;
    const existsAsync = promisify(exists);
    const mkdirAsync = promisify(mkdir);

    if (!(await existsAsync(rootPath))) {
      await mkdirAsync(rootPath, { recursive: true });
    }

    for (const environment of ALL_ENVIRONMENTS) {
      if (!(await existsAsync(rootPath + `/${environment}`))) {
        await mkdirAsync(rootPath + `/${environment}`);
      }
    }

    this.repositories[name] = repository;
    this.saveRepositories();

    return repository;
  }

  public getStreamFor(repository: Repository, path: string, offset = 0, environment = Environment.PRODUCTION) {
    let fullPath = this.configurationService.repositoriesPath + repository.name;
    fullPath += `/${environment}/`;
    fullPath += path;

    return createReadStream(fullPath, {
      start: offset
    });
  }

  public async getFileStats(repository: Repository, path: string, environment = Environment.PRODUCTION) {
    const statsAsync = promisify(stat);
    let fullPath = this.configurationService.repositoriesPath + repository.name;
    fullPath += `/${environment}/`;
    fullPath += path;
    const name = path.substring(path.lastIndexOf("/"));

    const stats = await statsAsync(fullPath);
    const item = repository.getItem(path);

    return new FileInfo(name, stats.size, item.hash);
  }

  public async buildRepository(repository: Repository, environment = Environment.PRODUCTION) {
    repository.state = RepositoryState.SCANNING;
    const repositoryPath = this.configurationService.repositoriesPath + repository.name;

    const task = this.taskService.createTask(new BuildRepositoryRunnable(repository, repositoryPath, environment));

    task.on("done", () => {
      writeFileSync(
        repositoryPath + `/${environment}.json`,
        JSON.stringify(repository.getEnvironmentItems(environment))
      );
      repository.state = RepositoryState.READY;
      this.saveRepositories();
    });
    task.start();

    return task;
  }

  public getRepositories() {
    return this.repositories;
  }
}
