import { Module } from "@nestjs/common";
import { RepositoryController } from "./Repository.controller";
import { RepositoryService } from "./Repository.service";
import { ConfigurationModule } from "../Configuration/ConfigurationModule";
import { TaskModule } from "../Task/Task.module";

@Module({
  providers: [RepositoryService],
  exports: [RepositoryService],
  controllers: [RepositoryController],
  imports: [ConfigurationModule, TaskModule]
})
export class RepositoryModule {}
