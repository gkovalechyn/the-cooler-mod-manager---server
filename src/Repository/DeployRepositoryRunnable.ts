import { Runnable } from "../Task/Runnable";
import { NotifyCallback } from "../Task/NotifyCallback";
import { Repository } from "./Repository";

export class DeployRepositoryRunnable extends Runnable {
  public constructor(private readonly repository: Repository) {
    super();
  }

  async run(notify: NotifyCallback) {
    // TODO
  }
}
