import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class ConfigurationService {
  public constructor(private readonly configService: ConfigService) {}

  public get serverPort() {
    return this.configService.get<number>("PORT", 3000);
  }

  public get isAuthEnabled() {
    return this.configService.get<boolean>("REQUIRE_AUTHENTICATION", false);
  }

  public get repositoriesPath() {
    return this.configService.get<string>("REPOSITORIES_PATH", "repositories/");
  }
}
