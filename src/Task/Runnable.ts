import { NotifyCallback } from "./NotifyCallback";

export abstract class Runnable {
  abstract async run(notify: NotifyCallback);
}
