import { TaskState } from "./TaskState";
import { Runnable } from "./Runnable";
import { EventEmitter } from "events";
import { Logger } from "@nestjs/common";

export class Task extends EventEmitter {
  private state = TaskState.WAITING;
  private logger = new Logger("Task");
  private messages: any[] = [];

  public constructor(private readonly id: number, private readonly runnable: Runnable) {
    super();
  }

  public start() {
    this.run();
  }

  private async run(): Promise<void> {
    this.state = TaskState.RUNNING;
    this.emit("start");

    try {
      const result = await this.runnable.run(this.onMessage.bind(this));
      this.state = TaskState.DONE;
      this.emit("done", result);
      return result;
    } catch (e) {
      this.state = TaskState.ERROR;
      this.logger.error(e);
      this.emit("error", e);
    }
  }

  private onMessage(message: any) {
    this.messages.push(message);
    this.emit("message", message);
  }

  public get State() {
    return this.state;
  }

  public get Id() {
    return this.id;
  }

  public get Messages() {
    return this.messages;
  }
}
