export enum TaskState {
  WAITING = "waiting",
  RUNNING = "running",
  DONE = "done",
  ERROR = "error"
}
