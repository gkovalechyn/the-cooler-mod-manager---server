import { Injectable } from "@nestjs/common";
import { Task } from "./Task";
import { Runnable } from "./Runnable";

@Injectable()
export class TaskService {
  private nextTaskId = 0;
  private tasks: Record<number, Task> = {};

  public createTask(runnable: Runnable) {
    const id = this.nextTaskId++;
    const task = new Task(id, runnable);

    this.tasks[id] = task;

    return task;
  }

  public getTask(id: number) {
    return this.tasks[id];
  }
}
