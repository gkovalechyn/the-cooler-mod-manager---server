import { Module } from "@nestjs/common";
import { TaskService } from "./Task.service";
import { TaskController } from "./Task.controller";

@Module({
  providers: [TaskService],
  exports: [TaskService],
  controllers: [TaskController]
})
export class TaskModule {}
