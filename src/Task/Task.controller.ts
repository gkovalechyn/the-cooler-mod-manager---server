import { Controller, Get, Param, BadRequestException } from "@nestjs/common";
import { TaskService } from "./Task.service";
import { TCMMResponse } from "../TCMMResponse";

@Controller("tasks")
export class TaskController {
  public constructor(private readonly taskService: TaskService) {}

  @Get(":id/status")
  public getStatus(
    @Param("id")
    id: number
  ) {
    const task = this.taskService.getTask(id);

    if (!task) {
      throw new BadRequestException();
    }

    return {
      id: task.Id,
      state: task.State,
      messaged: task.Messages
    };
  }
}
