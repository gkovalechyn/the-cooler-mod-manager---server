// Needed to be this name so it doesn't conflict with the default Response
export class TCMMResponse<T> {
  public constructor(data?: T) {
    this.data = data;
  }
  data: T;
}
