import { NestFactory } from "@nestjs/core";
import { AppModule } from "./App.module";
import { ValidationPipe } from "@nestjs/common";
import helmet from "helmet";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true
    })
  );

  app.enableCors();
  app.use(helmet());
  await app.listen(process.env.PORT);
}
bootstrap();
