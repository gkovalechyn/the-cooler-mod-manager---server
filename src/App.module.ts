import { Module } from "@nestjs/common";
import { AppController } from "./App.controller";
import { AppService } from "./App.service";
import { RepositoryModule } from "./Repository/Repository.module";
import { ManagementModule } from "./Management/Management.module";
import { ConfigModule } from "@nestjs/config";
import { ConfigurationModule } from "./Configuration/ConfigurationModule";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    ConfigurationModule,
    RepositoryModule,
    ManagementModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
